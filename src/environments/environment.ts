// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,
    firebase: {
        apiKey: 'AIzaSyBr4Vc7tljaGrZy0UDy0VjrOTOmEMtq8I4',
        authDomain: 'tracker-c2196.firebaseapp.com',
        databaseURL: 'https://tracker-c2196.firebaseio.com',
        projectId: 'tracker-c2196',
        storageBucket: 'tracker-c2196.appspot.com',
        messagingSenderId: '1026314674317'
    },
    api: 'https://api.chernov-sergey.ru/api/web'
    // api: 'http://localhost:3222'
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
