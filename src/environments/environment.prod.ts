export const environment = {
    production: true,
    firebase: {
        apiKey: 'AIzaSyBr4Vc7tljaGrZy0UDy0VjrOTOmEMtq8I4',
        authDomain: 'tracker-c2196.firebaseapp.com',
        databaseURL: 'https://tracker-c2196.firebaseio.com',
        projectId: 'tracker-c2196',
        storageBucket: 'tracker-c2196.appspot.com',
        messagingSenderId: '1026314674317'
    },
    api: 'https://api.chernov-sergey.ru/api/web'
};
