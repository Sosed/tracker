import { UserInterface } from './models/user.model';

export class Login {
    static readonly type = '[APP] Login';

    constructor( public payload: UserInterface ) {
    }
}

export class CreateUser {
    static readonly type = '[APP] CreateUser';

    constructor( public payload: UserInterface ) {
    }
}

export class UpdateToken {
    static readonly type = '[APP] Update access_token';

    constructor( public payload: string ) {
    }
}

export class Logout {
    static readonly type = '[APP] Logout';
}

// Events
export class LoginRedirect {
    static type = '[APP] LoginRedirect';
}