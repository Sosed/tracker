export interface UserInterface {
    id?: number;
    name?: string;
    access_token?: string;
    password?: string
}
