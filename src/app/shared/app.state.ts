import { Action, Selector, State, StateContext } from '@ngxs/store';
import {Navigate} from '@ngxs/router-plugin';
import {tap} from 'rxjs/operators';

import {Login, Logout, CreateUser, UpdateToken, LoginRedirect} from './app.actions';
import { AuthService } from '../auth/services/auth.service';
import { UserInterface } from './models/user.model';

export interface AppStateModel {
    user?: UserInterface;
    access_token?: string;
}

@State<AppStateModel>( {
    name: 'app',
    defaults: {
        user: {},
    }
} )
export class AppState {

    constructor( private authService: AuthService ) {
    }

    @Selector()
    static getToken( state: AppStateModel ) {
        return state.access_token;
    }

    @Selector()
    static getUser( state: AppStateModel ) {
        return state.user;
    }

    @Action( Login )
    login(ctx: StateContext<AppStateModel>, { payload }: Login) {
        const state = ctx.getState();
        ctx.patchState({
            ...state,
            user: payload,
            access_token: payload.access_token
        });
        ctx.dispatch(new Navigate(['/projects']));
    }

    @Action(LoginRedirect)
    onLoginRedirect(ctx: StateContext<AppStateModel>) {
        ctx.dispatch(new Navigate(['/auth/login']));
    }

    @Action( CreateUser )
    createUser({ getState, patchState }: StateContext<AppStateModel>, { payload }: CreateUser) {
        const state = getState();
        patchState({...state, user: payload });
    }

    @Action( Logout )
    logout( ctx: StateContext<AppStateModel> ) {
        ctx.patchState( {} );
        ctx.dispatch(new LoginRedirect());
    }

    @Action( UpdateToken )
    updateToken({ getState, patchState }: StateContext<AppStateModel>, { payload }: UpdateToken) {
        const state = getState();
        return this.authService.updateToken(payload).pipe(
            tap(user => {
                patchState( {
                    ...state,
                    access_token: user.access_token,
                    user: user
                } );
            })
        );
    }
}
