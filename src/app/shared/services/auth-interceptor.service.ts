import {
    HttpErrorResponse,
    HttpEvent,
    HttpHandler,
    HttpInterceptor,
    HttpRequest,
    HttpResponse
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { catchError, tap } from 'rxjs/operators';
import { throwError } from 'rxjs/internal/observable/throwError';
import { Observable } from 'rxjs/internal/Observable';
import { Select, Store } from '@ngxs/store';
import { AppState } from '../app.state';
import { Logout } from '../app.actions';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    @Select(AppState.getToken) token$: Observable<string>;
    auth_token = 'No set';

    constructor( private router: Router,
                 private store: Store ) {
        this.token$.subscribe( token => {
            this.auth_token = token;
        })
    }

    intercept( req: HttpRequest<any>, next: HttpHandler ): Observable<HttpEvent<any>> {
        const clonedRequest = this.setHeader(req, this.auth_token);
        return next.handle(clonedRequest).pipe(
            tap((event: HttpEvent<any>) => {
                if (event instanceof HttpResponse) {
                }
            }),
            catchError((err) => {
                console.error(err);
                if ( err instanceof HttpErrorResponse ) {
                    if ( err.status === 401 ) {
                        this.store.dispatch(new Logout);
                    }
                }
                return throwError(err);
            })
        );
    }

    setHeader(req: HttpRequest<any>, token) {
        const headers = {
            Authorization: `Bearer ${token}`
        };
        if (req.method === 'PUT' || req.method === 'POST') {
            headers['Content-type'] = 'application/json; charset=UTF-8';
            headers['Accept'] = 'application/json;';
        }
        return req.clone( {
            setHeaders: headers
        } );
    }
}
