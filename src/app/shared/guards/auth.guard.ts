import { Store } from '@ngxs/store';
import { CanActivate, Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { AppState } from '../app.state';

@Injectable( {
    providedIn: 'root'
} )
export class AuthGuard implements CanActivate {

    constructor( private store: Store,
                 private router: Router ) {
    }

    canActivate() {
        const token = this.store.selectSnapshot( AppState.getToken );
        const canActivate = token == 'undefined';
        // if ( canActivate ) {
        //     this.router.navigate(['/auth/login']);
        // }
        return !canActivate;
    }

}
