import { animate, animateChild, group, query, style, transition, trigger } from '@angular/animations';

export const ROUTE_AMIMATION = [
    trigger('routeAnimation', [
        transition('completed <=> all, active <=> all, active <=> completed', [
            query(':self', style({ height: '*', width: '*' })),
            query(':enter, :leave', style({ position: 'relative' })),
            query(':leave', style({ transform: 'scale(1)' })),
            query(':enter', style({ transform: 'scale(0)' })),
            group([
                query(':leave', group([
                    animate('0.4s cubic-bezier(.35,0,.25,1)', style({
                        transform: 'scale(0)'
                    })),
                    animateChild()
                ])),
                query(':enter', group([
                    animate('0.4s cubic-bezier(.35, 0, .25, 1)', style({
                        transform: 'scale(1)'
                    })),
                    animateChild()
                ]))
            ]),
            query(':self', style({ height: '*', width: '*' })),
        ])
    ])
];
