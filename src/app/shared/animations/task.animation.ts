import { animate, state, style, transition, trigger } from '@angular/animations';

export const TASK_ANIMATION = [
    trigger('taskAnimation', [
        state('init', style({
            opacity: 0,
            transform: 'translateX(30px)',
            'height': 0,
        })),
        state('loading', style({
            opacity: 0,
            transform: 'translateX(30px)',
            'height': '*',
        })),
        state('loaded', style({
            opacity: 1,
            transform: 'translateX(0)',
            'height': '*',
        })),
        state('remove', style({
            opacity: 0,
            transform: 'translateX(30px)',
            'height': 0,
        })),
        transition('init => loading', animate('200ms ease-out')),
        transition('loading => loaded', animate('200ms ease-out')),
        transition('loaded => remove', animate('200ms ease-out')),
    ])
];

