import { animate, style, transition, trigger } from '@angular/animations';

export const DROPDOWN_ANIMATION = [
    trigger(
        'dropdown', [
            transition( ':enter', [
                style( { opacity: 0.5, transform: 'translateY(-10px)' } ),
                animate( '.1s ease-out', style( { opacity: 1, transform: 'translateY(0)' } ) )
            ] ),
            transition( ':leave', [
                    style( { opacity: 1, transform: 'translateY(0)'} ),
                    animate( '.15s ease-out', style( {
                        opacity: 0.5,
                        transform: 'translateY(-10px)',
                    } ) )
                ]
            )
        ],
    ),
];
