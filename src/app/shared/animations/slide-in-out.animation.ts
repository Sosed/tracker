import { animate, style, transition, trigger } from '@angular/animations';

export const SLIDE_AMINATION = [
    trigger('slideAnimation', [
        transition( ':enter', [
            style( { opacity: 0.5, transform: 'translateX(40px)', 'height': 0 } ),
            animate( '.1s ease-out', style( { opacity: 1, transform: 'translateX(0)', 'height': '*' } ) )
        ] ),
        transition( ':leave', [
                style( { opacity: 1, transform: 'translateX(0)', 'height': '*' } ),
                animate( '.15s ease-out', style( {
                    opacity: 0.5,
                    transform: 'translateX(40px)',
                    'height': 0
                } ) )
            ]
        ),
    ])
];

