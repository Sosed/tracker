import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './containers/login/login.component';
import { RegComponent } from './containers/reg/reg.component';

const routes: Routes = [
    {
        path: '',
        redirectTo: 'login'
    }, {
        path: 'login',
        component: LoginComponent
    }, {
        path: 'reg',
        component: RegComponent
    }
];

@NgModule( {
    imports: [
        RouterModule.forChild( routes )
    ],
    declarations: []
} )
export class AuthRouterModule {
}

export const authContainers = [
    LoginComponent,
    RegComponent
];
