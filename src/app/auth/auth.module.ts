import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { authContainers, AuthRouterModule } from './auth.routes';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

@NgModule( {
    imports: [
        CommonModule,
        AuthRouterModule,
        ReactiveFormsModule,
        RouterModule
    ],
    declarations: [
        ...authContainers
    ]
} )
export class AuthModule {
}
