import { ChangeDetectorRef, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { environment } from '../../../environments/environment';
import { UserInterface } from '../../shared/models/user.model';
import { Router } from '@angular/router';

@Injectable( {
    providedIn: 'root'
} )
export class AuthService {

    api = `${environment.api}/v1`;
    private authKey = 'auth.token';

    constructor(private httpClient: HttpClient,
                private router: Router) {
    }

    login(user: UserInterface ): Observable<UserInterface> {
        return this.httpClient.post<UserInterface>(`${this.api}/users/login`, user);
    }

    register(user: UserInterface ): Observable<UserInterface> {
        return this.httpClient.post<UserInterface>(`${this.api}/users`, user);
    }

    updateToken(token: string ): Observable<UserInterface> {
        return this.httpClient.post<UserInterface>(`${this.api}/users/token`, {
            access_token: token
        });
    }

    logout() {
        localStorage.removeItem(this.authKey);
        this.router.navigate( [ '/auth/login' ] );
    }
}
