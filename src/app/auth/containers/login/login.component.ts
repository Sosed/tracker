import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngxs/store';
import { UserInterface } from '../../../shared/models/user.model';
import { AuthService } from '../../services/auth.service';
import { Observable } from 'rxjs/internal/Observable';
import { Login } from '../../../shared/app.actions';

@Component( {
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: [ './login.component.scss' ]
} )
export class LoginComponent implements OnInit {

    form: FormGroup = new FormGroup( {
        username: new FormControl( null, [ Validators.required ] ),
        password: new FormControl( null, [ Validators.required ] ),
    } );

    errorMessage: string;

    constructor(private store: Store,
                private authService: AuthService) {
    }

    ngOnInit() {
    }

    submit() {
        this.errorMessage = '';
        this.authService.login(<UserInterface>{
            name: this.form.controls.username.value,
            password: this.form.controls.password.value
        }).subscribe((result) => {
            this.store.dispatch(new Login(result));
        }, (err: Observable<never>) => {
            this.errorMessage = 'The data can not be processed on the server. Try later';
            if (err['status'] === 404 || err['status'] === 403) {
                this.errorMessage = 'Incorrect login or password';
            }
        });
    }

    hasError( attribute: string ) {
        return this.form.controls[ attribute ].touched && !this.form.controls[ attribute ].valid;
    }

}
