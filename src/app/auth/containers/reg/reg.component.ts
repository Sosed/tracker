import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { Login } from '../../../shared/app.actions';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Store } from '@ngxs/store';
import { AuthService } from '../../services/auth.service';
import { UserInterface } from '../../../shared/models/user.model';

@Component({
  selector: 'app-reg',
  templateUrl: './reg.component.html',
  styleUrls: ['./reg.component.scss']
})
export class RegComponent implements OnInit {

    form: FormGroup = new FormGroup( {
        username: new FormControl( null, [ Validators.required ] ),
        email: new FormControl( null, [ Validators.required, Validators.email ] ),
        password: new FormControl( null, [ Validators.required ] ),
    } );

    errorMessage: string;

    constructor(private store: Store,
                private authService: AuthService,
                private router: Router) {
    }

    ngOnInit() {
    }

    submit() {
        this.errorMessage = '';
        this.authService.register(<UserInterface>{
            name: this.form.controls.username.value,
            email: this.form.controls.email.value,
            password: this.form.controls.password.value,
        }).subscribe((result) => {
            this.store.dispatch(new Login(result));
            this.router.navigate( [ '/projects' ] )
        }, (err: Observable<never>) => {
            this.errorMessage = 'The data can not be processed on the server. Try later';
            if (err['status'] === 404 || err['status'] === 403) {
                this.errorMessage = 'Incorrect login or password';
            } else if (err['status'] === 422 && err['error']) {
                this.errorMessage = err['error'].map(e => {
                    return `<div>${e.message}</div>`;
                }).join('');
            }
        });
    }

    hasError( attribute: string ) {
        return this.form.controls[ attribute ].touched && !this.form.controls[ attribute ].valid;
    }

}
