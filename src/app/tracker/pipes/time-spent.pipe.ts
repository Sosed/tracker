import { Pipe, PipeTransform } from '@angular/core';

@Pipe( {
    name: 'timeSpent',
    pure: false
} )
export class TimeSpentPipe implements PipeTransform {

    constructor() {
    }

    transform( value: number ) {
        const m = ~~(value / 1000 / 60) % 60;
        const h = ~~(value / 1000 / 60 / 60);
        return h + 'h ' + m + 'm';
    }
}
