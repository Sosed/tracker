import { TimerPipe } from './timer.pipe';
import { TimeIntervalPipe } from './time-interval.pipe';
import { ReversePipe } from './reverse.pipe';
import { TimeSpentPipe } from './time-spent.pipe';

export const pipes: any[] = [
    TimerPipe,
    TimeIntervalPipe,
    ReversePipe,
    TimeSpentPipe
];
