import { ChangeDetectorRef, NgZone, OnDestroy, Pipe, PipeTransform } from '@angular/core';

@Pipe( {
    name: 'timer',
    pure: false
} )
export class TimerPipe implements PipeTransform, OnDestroy {
    private timer: number;

    constructor( private changeDetectorRef: ChangeDetectorRef, private ngZone: NgZone ) {
    }

    transform( value: number ) {
        this.removeTimer();
        const d = new Date( +value );
        const now = new Date();
        const seconds = Math.round( Math.abs( (now.getTime() - d.getTime()) / 1000 ) );
        this.timer = this.ngZone.runOutsideAngular( () => {
            if ( typeof window !== 'undefined' ) {
                return window.setTimeout( () => {
                    this.ngZone.run( () => this.changeDetectorRef.markForCheck() );
                }, 1000 );
            }
            return null;
        } );
        return TimerPipe.getTimerValue(seconds);
    }

    ngOnDestroy(): void {
        this.removeTimer();
    }

    private removeTimer() {
        if ( this.timer ) {
            window.clearTimeout( this.timer );
            this.timer = null;
        }
    }

    static getTimerValue(seconds) {
        const minutes = Math.round( Math.abs( seconds / 60 ) );
        const hours = Math.floor( Math.abs( minutes / 60 ) );
        if ( Number.isNaN( seconds ) ) {
            return '';
        } else if ( seconds < 60 ) {
            return ['00', '00', TimerPipe.correctTime(seconds, 60)].join(':');
        } else if ( minutes < 60 ) {
            return ['00', TimerPipe.correctTime(minutes, 60), TimerPipe.correctTime(seconds, 60)].join(':');
        } else {
            return [TimerPipe.correctTime(hours), TimerPipe.correctTime(minutes, 60), TimerPipe.correctTime(seconds, 60)].join(':');
        }
    }

    static correctTime(value, maxValue = 0) {
        let v = value;
        if (maxValue) {
            v = value % maxValue;
        }
        return v < 10 ? '0' + v : v;
    }
}
