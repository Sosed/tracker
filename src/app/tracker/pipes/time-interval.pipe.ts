import { Pipe, PipeTransform } from '@angular/core';
import { TimeInterval } from '../models/task.model';
import { TimerPipe } from './timer.pipe';

@Pipe( {
    name: 'timerInterval',
    pure: false
} )
export class TimeIntervalPipe implements PipeTransform {

    constructor() {
    }

    transform( interval: TimeInterval ) {
        const start = new Date( +interval.start_timer );
        const stop = new Date( +interval.stop_timer );
        const seconds = Math.round( Math.abs( (stop.getTime() - start.getTime()) / 1000 ) );
        return TimerPipe.getTimerValue(seconds);
    }
}
