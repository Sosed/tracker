import { TaskInterface } from '../models/task.model';

export class LoadTasks {
    static readonly type = '[TASK] load';

    constructor(public payload: number) {}
}

export class AddTask {
    static readonly type = '[TASK] Add';

    constructor(public payload: TaskInterface) { }
}

export class AddMultiTask {
    static readonly type = '[TASK] AddMultiTask';

    constructor(public payload: TaskInterface) { }
}

export class UpdateTask {
    static readonly type = '[TASK] Update';

    constructor(public payload: TaskInterface) { }
}

export class StopTask {
    static readonly type = '[TASK] Stop';

    constructor(public payload: TaskInterface) { }
}

export class RemoveTask {
    static readonly type = '[TASK] Remove';

    constructor(public payload: TaskInterface) {}
}
