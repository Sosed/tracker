import { ProjectInterface } from '../models/project.model';

export class LoadProjects {
    static readonly type = '[PROJECT] Load projects';
}

export class AddProject {
    static readonly type = '[PROJECT] Add';

    constructor(public payload: ProjectInterface) { }
}

export class UpdateProject {
    static readonly type = '[PROJECT] Update';

    constructor(public payload: ProjectInterface) { }
}

export class RemoveProject {
    static readonly type = '[PROJECT] Remove';

    constructor(public payload: ProjectInterface) {}
}

export class RestoreProject {
    static readonly type = '[PROJECT] Restore';

    constructor() {}
}

export class ClearRemovedProject {
    static readonly type = '[PROJECT] Clear removed project';

    constructor() {}
}