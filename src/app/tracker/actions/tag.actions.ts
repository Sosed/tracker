import {TagInterface} from '../models/tag.model';

export class LoadTags {
    static readonly type = '[TAG] load';

    constructor() {}
}

export class AddTag {
    static readonly type = '[TAG] Add';

    constructor(public payload: string) { }
}


export class UpdateTag {
    static readonly type = '[TAG] Update';

    constructor(public payload: TagInterface) { }
}

export class RemoveTag {
    static readonly type = '[TAG] Remove';

    constructor(public payload: TagInterface) { }
}
