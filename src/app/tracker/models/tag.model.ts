
export interface TagInterface {
    id?: number;
    name: string;
    user_id?: number;
    active?: boolean;
}
