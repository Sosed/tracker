import { TaskInterface } from './task.model';

export interface TaskGroupInterface {
    date: string;
    timerSum: number;
    tasks: TaskInterface[];
}
