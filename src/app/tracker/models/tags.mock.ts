export enum SkillType {
    frontend = 'Frontend',
    backend = 'Backend',
}

export interface TagInterface {
    name: string;
    active?: boolean;
}
export interface TagListInterface {
    groupName: SkillType;
    tags: TagInterface[];
}

export const TAGS: TagListInterface[] = [
    {
        groupName: SkillType.frontend,
        tags: [
            { name: 'Angular' },
            { name: 'SCSS' },
            { name: 'HTML' },
            { name: 'Pug' },
        ]
    },
    {
        groupName: SkillType.backend,
        tags: [
            { name: 'Yii2' },
            { name: 'PHP' },
            { name: 'Java' },
            { name: 'Groovy' },
            { name: 'Spring' },
        ]
    },

];
