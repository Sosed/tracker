import { TaskInterface } from './task.model';

export enum ProjectStatus {
    ACTIVE = 10,
    DELETED = 20
}

export interface ProjectInterface {
    id?: number;
    name: string;
    user_id?: number;
    alias?: string;
    color?: string;
    status?: ProjectStatus;
    created_at?: number;
    updated_at?: number;
    tags?: string[] | string;
    tasks?: TaskInterface[];
}
