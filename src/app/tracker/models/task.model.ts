export interface TimeInterval {
    start_timer?: number;
    stop_timer?: number;
}

export interface TaskInterface extends TimeInterval {
    id?: number;
    project_id: number;
    parent_id?: number;
    name: string;
    description?: string;
    color_project?: string;
    isNew?: boolean;
}
