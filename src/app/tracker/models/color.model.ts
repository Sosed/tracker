export const COLORS = [
    { key: 'pink', value: '#F368E0'},
    { key: 'orange', value: '#FF9F43'},
    { key: 'red', value: '#EE5353'},
    { key: 'blue', value: '#0ABDE3'},
    { key: 'green', value: '#10AC84'},
    { key: 'green-pine', value: '#00D2D3'},
    { key: 'blue-dark', value: '#54A0FF'},
    { key: 'purple', value: '#7f49e8'},
];

export const colorEnum = COLORS.reduce( (obj, item) => {
    obj[item.key] = item.value;
    return obj;
}, {});
