import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MultiTaskFormComponent } from './multi-task-form.component';

describe('MultiTaskFormComponent', () => {
  let component: MultiTaskFormComponent;
  let fixture: ComponentFixture<MultiTaskFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MultiTaskFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MultiTaskFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
