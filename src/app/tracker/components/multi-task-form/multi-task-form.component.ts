import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';
import { TaskInterface } from '../../models/task.model';

@Component({
  selector: 'app-multi-task-form',
  templateUrl: './multi-task-form.component.html',
  styleUrls: ['./multi-task-form.component.scss']
})
export class MultiTaskFormComponent implements OnInit {

    @Input() task: TaskInterface;
    @Input() projectId: number;
    @Output() createTasks = new EventEmitter<TaskInterface[]>();

    formGroup: FormGroup;

    ngOnInit(): void {
        this.formGroup = new FormGroup ({
            name: new FormControl('test'),
            description: new FormControl('testdescr'),
            workLogs: new FormControl('3,5,6'),
            start: new FormControl('18.6'),
        });

    }

    create() {
        const controls = this.formGroup.controls;
        const values: string = controls['workLogs'].value;
        let tasks: TaskInterface[] = [];
        if (values) {
            const workLogs = values.split(',');
            const startDateValue = controls.start.value.split('.');
            const startDate = new Date(2018, startDateValue[1], startDateValue[0], 9).getTime();
            console.log(new Date(startDate));
            const daySeconds = 24 * 60 * 60 * 1000;
            workLogs.forEach((log, day) => {
                // Начинаем таски в новый день с 9 до 11 часов
                let h = startDate + (day * daySeconds) + this.getRandomInt(0, 2 * 60 * 60 * 1000);
                if (+log === 0) {
                    // Переходим на следующий день
                    h += +daySeconds;
                    console.log('---');
                    return;
                }
                // Переходим на следующий день
                const r = this.getRandMilliseconds(+log);
                console.log({
                    start_timer: new Date(h),
                    stop_timer: new Date(h + r),
                    r: log
                });
                tasks.push({
                    name: controls.name.value,
                    description: controls.description.value,
                    project_id: this.projectId,
                    start_timer: h,
                    stop_timer: Math.floor(h + r)
                });
            });

        }
        this.createTasks.emit(tasks);
    }

    /**
     * Получаем рандомное число милисекунд
     * @param {number} h
     * @return {number} - плюс минус пол часа от h в милисекундом
     */
    private getRandMilliseconds(h: number) {
        return (h + Math.random()) * 60 * 60 * 1000;
    }

    private getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min)) + min;
    }
}
