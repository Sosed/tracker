import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ProjectInterface } from '../../models/project.model';
import { COLORS } from '../../models/color.model';

@Component( {
    selector: 'app-project-form',
    templateUrl: './project-form.component.html',
    styleUrls: [ './project-form.component.scss' ],
    changeDetection: ChangeDetectionStrategy.OnPush,
} )
export class ProjectFormComponent implements OnInit {

    @Input() project: ProjectInterface;
    @Output() submit = new EventEmitter<ProjectInterface>();
    @Output() remove = new EventEmitter<ProjectInterface>();
    colors = COLORS;
    selectedColor: string;
    isAnimated = true;

    constructor() {
    }

    ngOnInit() {
        if ( !this.project ) {
            this.project = {
                name: '',
                color: 'red'
            };
        }
        this.updateSelectedColor();
        setTimeout(() => {
            this.isAnimated = false;
        }, 1000);
    }

    handleSubmit( name: string ) {
        this.project.name = name;
        this.project.color = this.colors.find( color => color.value === this.selectedColor ).key;
        this.submit.emit( this.project );
    }

    handleRemove() {
        this.remove.emit( this.project );
    }

    updateSelectedColor() {
        this.selectedColor = this.colors.find( color => color.key === this.project.color ).value;
    }

    isEditMode() {
        return this.project && this.project.id;
    }
}
