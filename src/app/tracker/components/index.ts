import {ProjectFormComponent} from './project-form/project-form.component';
import {MultiTaskFormComponent} from './multi-task-form/multi-task-form.component';
import {TaskFormComponent} from './task-form/task-form.component';
import {HeaderComponent} from './header/header.component';
import {LoaderComponent} from './loader/loader.component';
import {TagComponent} from './tag/tag.component';
import {TagManagerComponent} from './tag/tag-manager/tag-manager.component';

export const components = [
    ProjectFormComponent,
    TaskFormComponent,
    MultiTaskFormComponent,
    TagComponent,
    LoaderComponent,
    HeaderComponent,
    TagManagerComponent
];
