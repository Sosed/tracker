import {Component} from '@angular/core';
import {Logout} from '../../../shared/app.actions';
import {AppState} from '../../../shared/app.state';
import {Observable} from 'rxjs/internal/Observable';
import {UserInterface} from '../../../shared/models/user.model';
import {Select, Store} from '@ngxs/store';
import {DROPDOWN_ANIMATION} from '../../../shared/animations/dropdown.animation';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss'],
    animations: DROPDOWN_ANIMATION
})
export class HeaderComponent {

    @Select(AppState.getUser) user$: Observable<UserInterface>;
    isOpenProfile = false;

    constructor(private store: Store) {
    }

    logout() {
        this.isOpenProfile = false;
        this.store.dispatch(new Logout());
    }

}
