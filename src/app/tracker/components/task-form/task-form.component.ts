import { ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TaskInterface } from '../../models/task.model';
import {FormControl, FormGroup} from '@angular/forms';
import { debounceTime } from 'rxjs/operators';
import { TASK_ANIMATION } from '../../../shared/animations/task.animation';
import { timer } from 'rxjs/internal/observable/timer';
import { DROPDOWN_ANIMATION } from '../../../shared/animations/dropdown.animation';

@Component( {
    selector: 'app-task-form',
    templateUrl: './task-form.component.html',
    styleUrls: [ './task-form.component.scss' ],
    animations: [...TASK_ANIMATION, ...DROPDOWN_ANIMATION]
} )
export class TaskFormComponent implements OnInit {

    @Input() task: TaskInterface;
    loadStatus;
    @Output() update = new EventEmitter<TaskInterface>();
    @Output() stop = new EventEmitter<TaskInterface>();
    @Output() play = new EventEmitter<TaskInterface>();
    @Output() remove = new EventEmitter<TaskInterface>();

    formGroup: FormGroup;
    isOpenOptions = false;

    constructor(private cdr: ChangeDetectorRef) {}

    ngOnInit(): void {
        this.formGroup = new FormGroup ({
            name: new FormControl(this.task.name),
            description: new FormControl(this.task.description)
        });
        this.setStatus('init');
        timer(200).subscribe(() => {
            this.setStatus('loading');
        });
        timer(400).subscribe(() => {
            this.setStatus('loaded');
            this.task.isNew = false;
        });

        this.formGroup.valueChanges.pipe(
            debounceTime(1500)
        ).subscribe(values => {
            this.task.name = values.name;
            this.task.description = values.description;
            this.update.emit(this.task);
        });
    }

    handlePlay() {
        this.play.emit(this.task);
    }

    handleStop() {
        this.stop.emit(this.task);
    }

    handleRemove() {
        this.loadStatus = 'loaded';
        this.isOpenOptions = false;
        timer(100).subscribe(() => {
            this.loadStatus = 'remove';
            this.cdr.detectChanges();
        });
        timer(350).subscribe(() => {
            this.remove.emit(this.task);
        });
    }

    private setStatus(status) {
        if (this.task.isNew) {
            this.loadStatus = status;
            this.cdr.detectChanges();
        }
    }
}
