import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { DROPDOWN_ANIMATION } from '../../../shared/animations/dropdown.animation';
import { debounceTime } from 'rxjs/operators';
import { FormControl, FormGroup } from '@angular/forms';
import {timer} from 'rxjs';
import { Select } from '@ngxs/store';
import { TagState } from '../../state/tag.state';
import { Observable } from 'rxjs/internal/Observable';
import { TagInterface } from '../../models/tag.model';

@Component( {
    selector: 'app-tag',
    templateUrl: './tag.component.html',
    styleUrls: [ './tag.component.scss' ],
    animations: DROPDOWN_ANIMATION
} )
export class TagComponent implements OnChanges {

    @Input() values = [];
    @Output() updateTags = new EventEmitter<string[]>();
    @Select(TagState.getTags) tags$: Observable<TagInterface[]>;
    private needSave = false;
    tagsList = [];
    isOpen = false;
    isAdded = false;
    isAnimated = true;
    isLoaded = false;
    formGroup: FormGroup;

    constructor() {
    }

    ngOnChanges() {
        if (!this.values) {
            this.values = [];
        }
        this.isAnimated = true;
        this.isLoaded = true;
        timer(1000).subscribe(() => {
            this.isAnimated = false;
        });
        this.tags$.subscribe(tags => {
            this.tagsList = tags;
        });
        this.formGroup = new FormGroup ({
            values: new FormControl(this.values),
        });
        this.updateTagList();
        this.formGroup.valueChanges.pipe(
            debounceTime(1500)
        ).subscribe(() => {
            this.updateTags.emit(this.values);
        });
    }

    closeDropdown() {
        if ( !this.isAdded ) {
            this.isOpen = false;
        }
    }

    addTag( tag: TagInterface ) {
        tag.active = !tag.active;
        this.isAdded = true;
        if (tag.active) {
            this.values = this.values.filter( item => item !== tag.name );
        } else {
            this.values.push( tag.name );
        }
        this.updateTagList();
        setTimeout( () => {
            this.isAdded = false;
        }, 10 );
    }

    removeTag( tagName: string ) {
        this.values = this.values.filter( item => item !== tagName );
        this.updateTagList();
    }

    private updateTagList(save = true) {
        this.tagsList = this.tagsList.map( tag => {
            tag.active = this.values.indexOf( tag.name ) === -1;
            return tag;
        } );
        this.needSave = save;
        if (save) {
            this.formGroup.controls['values'].setValue( this.values );
        }
    }
}
