import {ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {debounceTime} from 'rxjs/operators';
import {timer} from 'rxjs/internal/observable/timer';
import {TagInterface} from '../../../models/tag.model';
import {DROPDOWN_ANIMATION} from '../../../../shared/animations/dropdown.animation';
import {TASK_ANIMATION} from '../../../../shared/animations/task.animation';

@Component({
    selector: 'app-tag-manager',
    templateUrl: './tag-manager.component.html',
    styleUrls: ['./tag-manager.component.scss'],
    animations: [...TASK_ANIMATION, ...DROPDOWN_ANIMATION]
})
export class TagManagerComponent implements OnInit {

    @Input() tag: TagInterface;
    @Output() update = new EventEmitter<TagInterface>();
    @Output() remove = new EventEmitter<TagInterface>();

    loadStatus;

    formGroup: FormGroup;
    isOpenOptions = false;

    constructor(private cdr: ChangeDetectorRef) {}

    ngOnInit(): void {
        this.formGroup = new FormGroup ({
            name: new FormControl(this.tag.name)
        });

        this.formGroup.valueChanges.pipe(
            debounceTime(1500)
        ).subscribe(values => {
            this.tag.name = values.name;
            this.update.emit(this.tag);
        });
    }

    handleRemove() {
        this.loadStatus = 'loaded';
        this.isOpenOptions = false;
        timer(100).subscribe(() => {
            this.loadStatus = 'remove';
            this.cdr.detectChanges();
        });
        timer(350).subscribe(() => {
            this.remove.emit(this.tag);
        });
    }
}
