import {NgModule} from '@angular/core';
import {CommonModule, DatePipe} from '@angular/common';
import {RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ClickOutsideModule} from 'ng-click-outside';
import {NgxChartsModule} from '@swimlane/ngx-charts';
import {NgxsModule} from '@ngxs/store';


import {trackerContainers, TrackerRouterModule} from './tracker.routes';
import {ProjectState} from './state/project.state';
import {TagState} from './state/tag.state';

import {pipes} from './pipes';
import {components} from './components';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        NgxsModule.forFeature([
            ProjectState,
            TagState,
        ]),
        FormsModule,
        TrackerRouterModule,
        ReactiveFormsModule,
        ClickOutsideModule,
        NgxChartsModule
    ],
    declarations: [
        ...trackerContainers,
        ...pipes,
        ...components
    ],
    providers: [
        DatePipe
    ]
})
export class TrackerModule {
}
