import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ProjectInterface } from '../models/project.model';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs/internal/Observable';
import { TaskInterface } from '../models/task.model';
import { map, switchMap } from 'rxjs/operators';
import { Select } from '@ngxs/store';
import { AppState } from '../../shared/app.state';
import { UserInterface } from '../../shared/models/user.model';

@Injectable( {
    providedIn: 'root'
} )
export class ProjectService {

    api = `${environment.api}/v1`;
    @Select(AppState.getUser) user$: Observable<UserInterface>;

    constructor(private http: HttpClient) {
    }

    getProjects(): Observable<ProjectInterface[]> {
        return this.http.get<ProjectInterface[]>(`${this.api}/projects`, {
            params: {
                'expand': 'tasks',
            }
        }).pipe(
            map(projects => {
                return projects.map((project: any) => {
                    project.tags = JSON.parse(project.tags) || [];
                    return project;
                });
            })
        );
    }

    createProjects(project: ProjectInterface): Observable<ProjectInterface> {
        project.alias = project.name;
        return this.user$.pipe(
            switchMap( user => {
                project.user_id = user.id;
                return this.http.post<ProjectInterface>( `${this.api}/projects`, project )
            })
        );
    }

    updateProject(project: ProjectInterface): Observable<ProjectInterface> {
        return this.http.put<ProjectInterface>(`${this.api}/projects/${project.id}`, {
            ...project,
            tags: JSON.stringify(project.tags)
        });
    }

    createTask(task: TaskInterface): Observable<TaskInterface> {
        return this.http.post<TaskInterface>(`${this.api}/tasks`, task);
    }

    updateTask(task: TaskInterface): Observable<TaskInterface> {
        return this.http.put<TaskInterface>(`${this.api}/tasks/${task.id}`, task);
    }

    removeTask(task: TaskInterface): Observable<TaskInterface> {
        return this.http.delete<TaskInterface>(`${this.api}/tasks/${task.id}`);
    }
}
