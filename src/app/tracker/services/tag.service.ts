import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/internal/Observable';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {TagInterface} from '../models/tag.model';
import {Select} from '@ngxs/store';
import {UserInterface} from '../../shared/models/user.model';
import {AppState} from '../../shared/app.state';
import {switchMap} from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class TagService {

    api = `${environment.api}/v1`;
    @Select(AppState.getUser) user$: Observable<UserInterface>;

    constructor(private http: HttpClient) {
    }

    getTags(): Observable<TagInterface[]> {
        return this.http.get<TagInterface[]>(`${this.api}/tags`);
    }

    createTag(tag: TagInterface): Observable<TagInterface> {
        return this.user$.pipe(
            switchMap(user => {
                tag.user_id = user.id;
                return this.http.post<TagInterface>(`${this.api}/tags`, tag);
            })
        );
    }

    updateTag(tag: TagInterface): Observable<TagInterface> {
        return this.http.put<TagInterface>(`${this.api}/tags/${tag.id}`, tag);
    }

    removeTag(tag: TagInterface): Observable<TagInterface> {
        return this.http.delete<TagInterface>(`${this.api}/tags/${tag.id}`);
    }
}
