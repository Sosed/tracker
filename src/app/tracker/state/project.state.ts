import { ProjectInterface, ProjectStatus } from '../models/project.model';
import { Action, Selector, State, StateContext } from '@ngxs/store';
import {
    AddProject,
    ClearRemovedProject,
    LoadProjects,
    RemoveProject,
    RestoreProject,
    UpdateProject
} from '../actions/project.actions';
import { ProjectService } from '../services/project.service';
import { debounceTime, shareReplay, tap } from 'rxjs/operators';
import { AddMultiTask, AddTask, RemoveTask, StopTask, UpdateTask } from '../actions/task.actions';
import { TaskInterface } from '../models/task.model';
import { Navigate } from '@ngxs/router-plugin';

export class ProjectStateModel {
    projects: ProjectInterface[];
    currentTask: TaskInterface;
    removedProject?: ProjectInterface;
    isLoaded: boolean;
}

@State<ProjectStateModel>( {
    name: 'projects',
    defaults: {
        projects: [],
        currentTask: null,
        isLoaded: false
    }
} )
export class ProjectState {

    constructor( private projectService: ProjectService ) {
    }

    @Selector()
    static getProjects( state: ProjectStateModel ) {
        return state.projects.filter( project => project.status === ProjectStatus.ACTIVE );
    }

    @Selector()
    static getRemovedProject( state: ProjectStateModel ) {
        return state.removedProject
    }

    @Selector()
    static getCurrentTask( state: ProjectStateModel ) {
        return state.currentTask;
    }

    @Action( LoadProjects )
    getProjects( { getState, patchState }: StateContext<LoadProjects> ) {
        const state = getState();
        return this.projectService.getProjects().pipe(
            tap( projects => {
                patchState( {
                    ...state,
                    projects: projects,
                    currentTask: this.getCurrentTask( projects ),
                    isLoaded: true
                } );
            } ),
            shareReplay()
        );
    }

    @Action( AddProject )
    add( { getState, patchState }: StateContext<ProjectStateModel>, { payload }: AddProject ) {
        const state = getState();
        return this.projectService.createProjects( payload ).subscribe( project => {
            payload.id = project.id;
            payload.status = ProjectStatus.ACTIVE;
            payload.tasks = [];
            patchState( {
                ...state,
                projects: [ ...state.projects, payload ]
            } );
        } );
    }

    @Action( UpdateProject )
    update( { getState, patchState }: StateContext<ProjectStateModel>, { payload }: UpdateProject ) {
        const state = getState();
        patchState( {
            ...state,
            projects: state.projects.map( project => {
                if ( +project.id === +payload.id ) {
                    this.projectService.updateProject( payload ).subscribe();
                    return payload;
                }
                return project;
            } )
        } );
    }

    @Action( RemoveProject )
    remove( ctx: StateContext<ProjectStateModel>, { payload }: RemoveProject ) {
        const state = ctx.getState();
        if ( state.currentTask && +state.currentTask.project_id === +payload.id ) {
            /**
             * При удалении проекта останавливаем запущенную задачу
             */
            ctx.dispatch(new StopTask(state.currentTask));
        }
        ctx.patchState( {
            ...state,
            projects: state.projects.map( item => {
                if ( item.id === payload.id ) {
                    item.status = ProjectStatus.DELETED;
                    return item;
                }
                return item;
            } ),
            removedProject: payload
        } );
        payload.status = ProjectStatus.DELETED;
        ctx.dispatch( new Navigate( [ '/projects' ] ) );
        this.projectService.updateProject( payload ).subscribe(() => {
            ctx.dispatch(new Navigate(['/projects']));
        });
    }

    @Action( ClearRemovedProject )
    clearRemovedProject( ctx: StateContext<ProjectStateModel> ) {
        const state = ctx.getState();
        ctx.patchState( {
            ...state,
            removedProject: null
        } );
    }

    @Action( RestoreProject )
    restore( ctx: StateContext<ProjectStateModel> ) {
        const state = ctx.getState();
        ctx.patchState( {
            ...state,
            projects: state.projects.map( item => {
                if ( state.removedProject && item.id === state.removedProject.id ) {
                    item.status = ProjectStatus.ACTIVE;
                    this.projectService.updateProject( item ).subscribe();
                    return item;
                }
                return item;
            } ),
            removedProject: null
        } );
    }

    @Action( AddTask )
    addTask( { getState, setState }: StateContext<ProjectStateModel>, { payload }: AddTask ) {
        const state = getState();
        payload.start_timer = Date.now();
        payload.stop_timer = null;
        setState( {
            ...state,
            projects: [ ...state.projects.map( project => {
                this.stopTasks( project );
                if ( +project.id === +payload.project_id ) {
                    payload.isNew = true;
                    project.tasks.push( payload );
                }
                return project;
            } ) ],
            currentTask: this.getCurrentTask( state.projects, payload )
        } );
        this.projectService.createTask( payload ).pipe(
            debounceTime( 2000 )
        ).subscribe( ( task: TaskInterface ) => {
            payload.id = task.id;
        } );
    }

    @Action( AddMultiTask )
    addMultiTask( { getState, setState }: StateContext<ProjectStateModel>, { payload }: AddMultiTask ) {
        const state = getState();
        setState( {
            ...state,
            projects: [ ...state.projects.map( project => {
                if ( +project.id === +payload.project_id ) {
                    project.tasks.push( payload );
                }
                return project;
            } ) ],
            currentTask: this.getCurrentTask( state.projects, payload )
        } );
        this.projectService.createTask( payload ).subscribe( ( task: TaskInterface ) => {
            payload.id = task.id;
        } );
    }

    @Action( UpdateTask )
    updateTask( { getState, setState }: StateContext<ProjectStateModel>, { payload }: UpdateTask ) {
        const state = getState();
        setState( {
            ...state,
            projects: [ ...state.projects.map( project => {
                project.tasks.map( task => {
                    if ( task.id === payload.id ) {
                        return payload;
                    }
                    return task;
                } );
                return project;
            } ) ]
        } );
        this.projectService.updateTask( payload ).subscribe();
    }

    @Action( StopTask )
    stop( { getState, patchState }: StateContext<ProjectStateModel>, { payload }: StopTask ) {
        const state = getState();
        payload.stop_timer = Date.now();
        patchState( {
            ...state,
            projects: [ ...state.projects.map( project => {
                this.stopTasks( project );
                return project;
            } ) ],
            currentTask: null
        } );
    }

    @Action( RemoveTask )
    removeTask( { getState, patchState }: StateContext<ProjectStateModel>, { payload }: RemoveTask ) {
        const state = getState();
        patchState( {
            ...state,
            projects: [ ...state.projects.map( project => {
                if ( +project.id === +payload.project_id ) {
                    project.tasks = project.tasks.filter( item => item.id !== payload.id );
                }
                return project;
            } ) ]
        } );
        this.projectService.removeTask( payload ).subscribe();
    }

    private stopTasks( project ) {
        if ( !project ) {
            return;
        }
        project.tasks.map( item => {
            if ( !item.stop_timer ) {
                item.stop_timer = Date.now();
                this.projectService.updateTask( item ).subscribe();
            }
            return item;
        } );
    }

    private getCurrentTask( projects, task: TaskInterface = null ) {
        let currentTask: TaskInterface = task;
        projects.forEach( project => {
            if ( currentTask && currentTask.project_id ) {
                return;
            }
            currentTask = project.tasks.find( item => !item.stop_timer );
        } );
        if ( currentTask && currentTask.project_id ) {
            const project = projects.find( item => +item.id === +currentTask.project_id );
            currentTask.color_project = project ? project.color : null;
        }
        return currentTask;
    }
}
