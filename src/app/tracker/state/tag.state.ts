import {Action, Selector, State, StateContext} from '@ngxs/store';
import {shareReplay, tap} from 'rxjs/operators';
import {TagInterface} from '../models/tag.model';
import { AddTag, LoadTags, RemoveTag, UpdateTag } from '../actions/tag.actions';
import {TagService} from '../services/tag.service';

export class TagStateModel {
    isLoaded: boolean;
    tags: TagInterface[];
}

@State<TagStateModel>({
    name: 'tags',
    defaults: {
        isLoaded: false,
        tags: []
    }
})
export class TagState {

    constructor(private tagService: TagService) {
    }

    @Selector()
    static getTags(state: TagStateModel) {
        return state.tags;
    }

    @Action(LoadTags)
    loadTags({getState, patchState}: StateContext<LoadTags>) {
        const state = getState();
        return this.tagService.getTags().pipe(
            tap(tags => {
                patchState({
                    ...state,
                    tags: tags,
                    isLoaded: true
                });
            }),
            shareReplay()
        );
    }

    @Action(AddTag)
    addTag({getState, patchState}: StateContext<TagStateModel>, {payload}: AddTag) {
        const state = getState();
        return this.tagService.createTag({ name: payload }).subscribe(tag => {
            patchState({
                ...state,
                tags: [...state.tags, tag],
            });
        });
    }

    @Action(UpdateTag)
    updateTag({getState, patchState}: StateContext<TagStateModel>, {payload}: UpdateTag) {
        const state = getState();
        return this.tagService.updateTag(payload).subscribe(project => {
            payload.id = project.id;
            patchState({
                ...state,
                tags: [...state.tags.map(tag => {
                    if (+tag.id === +payload.id) {
                        tag = payload;
                    }
                    return tag;
                })],
            });
        });
    }
    @Action(RemoveTag)
    removeTag({getState, patchState}: StateContext<TagStateModel>, {payload}: RemoveTag) {
        const state = getState();
        patchState({
            ...state,
            tags: state.tags.filter(tag => tag.id !== payload.id),
        });
        return this.tagService.removeTag(payload).subscribe(tag => {

        });
    }
}
