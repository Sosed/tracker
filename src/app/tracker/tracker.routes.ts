import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ProjectsComponent} from './containers/projects/projects.component';
import {TaskListComponent} from './containers/task-list/task-list.component';
import {DashboardComponent} from './containers/dashboard/dashboard.component';
import {TagsComponent} from './containers/tags/tags.component';

const routes: Routes = [
    {
        path: '',
        component: ProjectsComponent,
        data: {title: 'Projects'},
        children: [{
            path: '',
            component: DashboardComponent
        }, {
            path: 'tags',
            component: TagsComponent
        }, {
            path: 'view/:id',
            component: TaskListComponent
        }]
    },
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    declarations: []
})
export class TrackerRouterModule {
}

export const trackerContainers = [
    ProjectsComponent,
    TaskListComponent,
    DashboardComponent,
    TagsComponent
];
