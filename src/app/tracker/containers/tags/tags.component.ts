import {Component, OnInit} from '@angular/core';
import {Select, Store} from '@ngxs/store';
import {Observable} from 'rxjs/internal/Observable';
import {TagState} from '../../state/tag.state';
import {POPOVER_RIGHT_ANIMATION} from '../../../shared/animations/popover-right.animation';
import {TagInterface} from '../../models/tag.model';
import { AddTag, RemoveTag, UpdateTag } from '../../actions/tag.actions';

@Component({
    selector: 'app-tags',
    templateUrl: './tags.component.html',
    styleUrls: ['./tags.component.scss'],
    animations: POPOVER_RIGHT_ANIMATION
})
export class TagsComponent implements OnInit {

    @Select(TagState.getTags) tags$: Observable<TagInterface[]>;
    isOpenCategoryForm = false;

    constructor(private store: Store) {
    }

    ngOnInit() {
    }

    createTag(name: string) {
        this.store.dispatch(new AddTag(name));
        this.isOpenCategoryForm = false;
    }

    removeTag(tag: TagInterface) {
        this.store.dispatch(new RemoveTag(tag));
    }

    updateTag(tag: TagInterface) {
        this.store.dispatch(new UpdateTag(tag));
    }
}
