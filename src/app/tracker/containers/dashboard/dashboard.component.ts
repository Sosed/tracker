import { Component, ElementRef, ViewChild } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { Select } from '@ngxs/store';
import { DatePipe } from '@angular/common';
import { colorEnum } from '../../models/color.model';
import { TaskGroupInterface } from '../../models/task-group.model';
import { ProjectInterface } from '../../models/project.model';
import { map } from 'rxjs/operators';
import { combineLatest } from 'rxjs';

interface LiteralInterface {
    name: string;
    value: string | number;
}

interface ChartInterface {
    name: string;
    series: LiteralInterface[];
}

@Component( {
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: [ './dashboard.component.scss' ]
} )
export class DashboardComponent {
    @Select( state => state.projects.projects ) project$: Observable<ProjectInterface[]>;
    @Select( state => state.projects.isLoaded ) isLoaded$: Observable<boolean>;

    @ViewChild( 'mainWrapper' )
    mainWrapper: ElementRef;

    chartWidth = 800;

    readonly values$: Observable<ChartInterface[]> = this.project$.pipe(
        map( projects => this.mapProjectsToGroups( projects ) ),
    );

    readonly tagValues$: Observable<LiteralInterface[]> = this.project$.pipe(
        map( projects => this.mapTagsToGroup( projects ) ),
    );

    readonly customColors$: Observable<LiteralInterface[]> = this.project$.pipe(
        map( projects => this.getCustomColors( projects ) )
    );

    readonly height$: Observable<number> = combineLatest( this.project$, this.values$, ( projects, values ) => {
        this.setChartWidth();
        return Math.max( 60 + 30 * projects.length, 60 + 30 * values.length );
    } );

    readonly tagChartHeight$: Observable<number> = combineLatest( this.project$, this.tagValues$, ( projects, values ) => {
        this.setChartWidth();
        return Math.max( 60 + 30 * projects.length, 60 + 30 * values.length );
    } );

    showProjects = true;

    constructor( private datePipe: DatePipe ) {
    }

    setChartWidth() {
        if ( this.mainWrapper ) {
            this.chartWidth = this.mainWrapper.nativeElement.offsetWidth;
        }
    }

    private mapTagsToGroup( projects: ProjectInterface[] ): LiteralInterface[] {
        const tags = {};
        projects.forEach( project => {
            if ( !Array.isArray( project.tags ) ) {
                return;
            }
            let totalTimeSpent = 0;
            project.tasks.forEach( task => {
                totalTimeSpent += (task.stop_timer ? task.stop_timer : Date.now()) - task.start_timer;
            } );
            project.tags.forEach( tag => {
                if ( !tags[ tag ] ) {
                    tags[ tag ] = {
                        value: 0
                    };
                }
                tags[ tag ].value += totalTimeSpent;
            } );
        } );
        return Object.keys( tags ).map( item => {
            return {
                name: item,
                value: this.millisecondsToHours( tags[ item ].value )
            }
        } );
    }

    private mapProjectsToGroups( projects: ProjectInterface[] ) {
        const days = {};
        projects.forEach( project => {
            const groups = this.groupTasks( project.tasks );
            groups.forEach( group => {
                if ( !days[ group.date ] ) {
                    days[ group.date ] = [];
                }
                days[ group.date ].push( {
                    name: project.name,
                    value: Math.min( 24, this.millisecondsToHours( group.timerSum ) )
                } );
            } );
        } );
        return Object.keys( days ).map( item => {
            return {
                name: item,
                series: days[ item ]
            }
        } );
    }

    private getCustomColors( projects: ProjectInterface[] ): LiteralInterface[] {
        return projects.map( project => ({
            name: project.name, value: colorEnum[ project.color ]
        }) );
    }

    private groupTasks( tasks ): TaskGroupInterface[] {
        if ( !tasks.length ) {
            return [];
        }
        const _groups = tasks.reduce( ( groups, task ) => {
            const date = new Date( +task.start_timer ).toISOString().split( 'T' )[ 0 ];
            if ( !groups[ date ] ) {
                groups[ date ] = [];
            }
            groups[ date ].push( task );
            return groups;
        }, {} );
        return Object.keys( _groups ).map( date => {
            const _tasks = _groups[ date ];
            let timerSum = 0;
            _tasks.forEach( task => {
                if ( task.stop_timer ) {
                    timerSum += task.stop_timer - task.start_timer;
                } else {
                    timerSum += Date.now() - task.start_timer;
                }
            } );
            return <TaskGroupInterface>{
                date: date,
                timerSum: timerSum,
                tasks: _tasks
            };
        } ).sort( ( a, b ) => {
            if ( new Date( a.date ) > new Date( b.date ) ) {
                return -1;
            }
            return 1;
        } ).map( item => {
            item.date = this.datePipe.transform( item.date, 'EEE, dd MMM' );
            return item;
        } );
    }

    millisecondsToHours( milliseconds: number ) {
        return +milliseconds / 1000 / 60 / 60
    }
}
