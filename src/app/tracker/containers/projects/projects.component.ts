import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { ProjectState } from '../../state/project.state';
import { Observable } from 'rxjs/internal/Observable';
import { ProjectInterface } from '../../models/project.model';
import { AddProject, ClearRemovedProject, LoadProjects, RestoreProject } from '../../actions/project.actions';
import { POPOVER_RIGHT_ANIMATION } from '../../../shared/animations/popover-right.animation';
import { TaskInterface } from '../../models/task.model';
import { StopTask } from '../../actions/task.actions';
import { SLIDE_AMINATION } from '../../../shared/animations/slide-in-out.animation';
import { LoadTags } from '../../actions/tag.actions';

@Component( {
    selector: 'app-projects',
    templateUrl: './projects.component.html',
    styleUrls: [ './projects.component.scss' ],
    changeDetection: ChangeDetectionStrategy.OnPush,
    animations: [...POPOVER_RIGHT_ANIMATION, ...SLIDE_AMINATION]
} )
export class ProjectsComponent implements OnInit {

    @Select(ProjectState.getProjects) projects$: Observable<ProjectInterface>;
    @Select(ProjectState.getRemovedProject) removedProjects$: Observable<ProjectInterface>;
    @Select(ProjectState.getCurrentTask) currentTask$: Observable<TaskInterface>;

    isOpenCreateProject = false;
    currentTask: TaskInterface;

    constructor(private store: Store) {
    }

    ngOnInit() {
        this.store.dispatch(new LoadProjects);
        this.store.dispatch(new LoadTags);
        this.currentTask$.subscribe(task => {
            this.currentTask = task;
        });
    }

    addProject(project: ProjectInterface) {
        this.store.dispatch(new AddProject({
            name: project.name,
            color: project.color
        }));
        this.isOpenCreateProject = false;
    }

    stopCurrentTask() {
        this.store.dispatch(new StopTask(Object.assign({}, this.currentTask)));
    }

    restoreProject() {
        this.store.dispatch(new RestoreProject());
    }

    confirmRemoveProject() {
        this.store.dispatch(new ClearRemovedProject());
    }
}
