import { ChangeDetectorRef, Component } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { Select, Store } from '@ngxs/store';
import { TaskInterface } from '../../models/task.model';
import { AddMultiTask, AddTask, RemoveTask, StopTask, UpdateTask } from '../../actions/task.actions';
import { TaskGroupInterface } from '../../models/task-group.model';
import { ProjectStateModel } from '../../state/project.state';
import { ActivatedRoute } from '@angular/router';
import { flatMap, map, switchMap, tap } from 'rxjs/operators';
import { of } from 'rxjs/internal/observable/of';
import { ProjectInterface } from '../../models/project.model';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { POPOVER_RIGHT_ANIMATION } from '../../../shared/animations/popover-right.animation';
import { RemoveProject, UpdateProject } from '../../actions/project.actions';
import { timer } from 'rxjs/internal/observable/timer';
import { TASK_ANIMATION } from '../../../shared/animations/task.animation';
import _ from 'lodash';

@Component( {
    selector: 'app-task-list',
    templateUrl: './task-list.component.html',
    styleUrls: [ './task-list.component.scss' ],
    animations: [ ...POPOVER_RIGHT_ANIMATION, ...TASK_ANIMATION ]
} )
export class TaskListComponent {

    @Select( state => state.projects ) project$: Observable<ProjectStateModel>;
    tasks$: BehaviorSubject<TaskInterface[]> = new BehaviorSubject( [] );
    currentProject: ProjectInterface;

    readonly groups$: Observable<TaskGroupInterface[]>;
    projectId: number;
    isOpenEditProject = false;
    loadStatus = 'init';

    constructor( private store: Store,
                 private route: ActivatedRoute,
                 private cdr: ChangeDetectorRef ) {
        this.groups$ = this.tasks$.pipe(
            flatMap( tasks => {
                return this.groupTasks( tasks );
            } )
        );

        this.route.params.pipe(
            tap( params => {
                this.projectId = params.id;
            } ),
            switchMap( () => {
                this.aminations();
                return this.project$;
            } ),
            map( stateProjects => {
                return stateProjects.projects;
            } ),
        ).subscribe( projects => {
            this.currentProject = projects.find( item => +item.id === +this.projectId );
            if ( this.currentProject && this.currentProject.tasks ) {
                this.tasks$.next( this.currentProject.tasks );
            }
        } );
    }

    groupTasks( tasks ): Observable<TaskGroupInterface[]> {
        if ( !tasks.length ) {
            return of( [] );
        }
        const _groups = tasks.reduce( ( groups, task ) => {
            const date = new Date( +task.start_timer ).toISOString().split( 'T' )[ 0 ];
            if ( !groups[ date ] ) {
                groups[ date ] = [];
            }
            groups[ date ].push( task );
            return groups;
        }, {} );
        return of( Object.keys( _groups ).map( date => {
            const _tasks = _groups[ date ];
            let timerSum = 0;
            _tasks.forEach( task => {
                if ( task.stop_timer ) {
                    timerSum += task.stop_timer - task.start_timer;
                } else {
                    timerSum += Date.now() - task.start_timer;
                }
            } );
            return <TaskGroupInterface>{
                date: date,
                timerSum: timerSum,
                tasks: _tasks
            };
        } ).sort( ( a, b ) => {
            if ( new Date( a.date ) > new Date( b.date ) ) {
                return -1;
            }
            return 1;
        } ) );
    }

    addTask() {
        this.store.dispatch( new AddTask( {
            description: '',
            name: '',
            project_id: this.projectId,
            stop_timer: 0
        } ) );
    }

    createTasks( tasks: TaskInterface[] ) {
        tasks.forEach( task => {
            this.store.dispatch( new AddMultiTask( task ) );
        } );
    }

    updateTask( task: TaskInterface ) {
        task.isNew = false;
        this.store.dispatch( new UpdateTask( task ) );
    }

    removeTask( task: TaskInterface ) {
        this.store.dispatch( new RemoveTask( _.cloneDeep( task ) ) );
    }

    handleStop( task ) {
        this.store.dispatch( new StopTask( _.cloneDeep( {
            ...task,
            isNew: false
        } ) ) );
    }

    handlePlay( task ) {
        this.store.dispatch( new AddTask( _.cloneDeep( {
            ...task,
            parent_id: task.parent_id ? task.parent_id : task.id,
            isNew: true
        } ) ) );
    }

    updateTags( tags: string[] ) {
        this.store.dispatch( new UpdateProject( _.cloneDeep( {
            ...this.currentProject,
            tags,
            isNew: false
        } ) ) );
    }

    updateProject( project: ProjectInterface ) {
        this.isOpenEditProject = false;
        this.store.dispatch( new UpdateProject( _.cloneDeep( {
            ...project,
            isNew: false
        } ) ) );
    }

    removeProject(project: ProjectInterface) {
        this.isOpenEditProject = false;
        this.store.dispatch( new RemoveProject( _.cloneDeep( {
            ...project,
            isNew: false
        } ) ) );
    }

    private aminations(): void {
        this.loadStatus = 'init';
        timer( 200 ).subscribe( () => {
            this.loadStatus = 'loading';
            this.cdr.detectChanges();
        } );

        timer( 400 ).subscribe( () => {
            this.loadStatus = 'loaded';
            this.cdr.detectChanges();
        } );
    }

}
