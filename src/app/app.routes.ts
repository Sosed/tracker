import { Routes } from '@angular/router';
import { AuthGuard } from './shared/guards/auth.guard';

export const APP_ROUTES: Routes = [
    { path: '', redirectTo: 'projects', pathMatch: 'full'},
    { path: 'projects', loadChildren: './tracker/tracker.module#TrackerModule', canActivate: [AuthGuard] },
    { path: 'auth', loadChildren: './auth/auth.module#AuthModule' },
];
