import { Component, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs/internal/Observable';
import { AppState } from './shared/app.state';
import { UpdateToken } from './shared/app.actions';

@Component( {
    selector: 'app-root',
    templateUrl: './app.component.html'
} )
export class AppComponent implements OnInit {

    @Select(AppState.getToken) token$: Observable<string>;

    constructor( private store: Store ) {
    }

    ngOnInit() {
        this.token$.subscribe(access_token => {
            this.store.dispatch(new UpdateToken(access_token));
        });
    }
}
